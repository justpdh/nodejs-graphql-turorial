var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  input MessageInput {
    content: String
    author: String
  }

  type Message {
    id: ID!
    content: String
    author: String
  }

  type Query {
    getMessage(id: ID!): Message
    getMessageList: [Message]
  }

  type Mutation {
    createMessage(input: MessageInput): Message
    updateMessage(id: ID!, input: MessageInput): Message
    clearMessage: Int!
  }
`);

// If Message had any complex fields, we'd put them on this object.
class Message {
    constructor(id, { content, author }) {
        this.id = id;
        this.content = content;
        this.author = author;
    }
}

// Maps username to content
var fakeDatabase = {
    '8ff6a8c42822425dcaab': {
        content: 'initial',
        author: 'aut'
    }
};

var root = {
    getMessage: ({ id }) => {
        console.log('getMessage');

        if (!fakeDatabase[id]) {
            throw new Error('no message exists with id ' + id);
        }
        return new Message(id, fakeDatabase[id]);
    },
    createMessage: ({ input }) => {
        // Create a random id for our "database".
        var id = require('crypto').randomBytes(10).toString('hex');

        fakeDatabase[id] = input;
        return new Message(id, input);
    },
    updateMessage: ({ id, input }) => {
        if (!fakeDatabase[id]) {
            throw new Error('no message exists with id ' + id);
        }
        // This replaces all old data, but some apps might want partial update.
        fakeDatabase[id] = input;
        return new Message(id, input);
    },
    getMessageList: () => {
        console.log('getMessageList');

        let rs = [];
        for(key in fakeDatabase){
            const msg = new Message(key, fakeDatabase[key]);
            rs.push(msg);
        }
        //rs.push(new Message('8ff6a8c42822425dcaab', fakeDatabase['8ff6a8c42822425dcaab']));

        return rs;
    },
    clearMessage: () => {
        console.log('clearMessage');

        fakeDatabase = {};

        return 0;
    },
};


var app = express();
app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
}));
app.listen(5000, () => {
    console.log('Running a GraphQL API server at localhost:5000/graphql');
});
/*
{
    "query" : "mutation {
        createMessage(input: {author: "andy", content: "hope is a good thing"}) {
            id, content, author
        }
    }"
}
{
    "query" : "mutation {
        updateMessage(id: "8ff6a8c42822425dcaab", input: {author: "aaa", content: "bbb"}) {
            id, content, author
        }
    }"
}
{
    "query" : "{
            getMessage(id: "8ff6a8c42822425dcaab") {
                id, content, author
            }
    }"
}
*/