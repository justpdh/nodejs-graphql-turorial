var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { buildSchema } = require('graphql');

var schema = buildSchema(`
  type Query {
    ip: String
  }
`);

const loggingMiddleware = (req, res, next) => {
    console.log('ip:', req.ip);
    next();
}

var root = {
    ip: function (args, request) {
        let rs = { 'ip': request.ip, 'arg': args, 'method': request.method };
        return JSON.stringify(rs);
        // console.log(request);
        // return request;
    }
};

var app = express();
app.use(loggingMiddleware);
app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
}));
app.listen(5000);
console.log('Running a GraphQL API server at localhost:5000/graphql');
/*
{
    "query" : "{
            ip
    }"
}
*/